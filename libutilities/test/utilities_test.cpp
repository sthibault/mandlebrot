//
// Created by steve on 9/10/19.
//
#include "libutilities/Utilities.h"
#include <gtest/gtest.h>
#include <iostream>

TEST(MapTest, SmokeTest) {
    long double newValue = map<long double>(50, 0, 100, 0.0, 10.0);
    ASSERT_NEAR(newValue, 5, 1e-17);
}

TEST(MapTest, NetagiveToPositiveTo) {
    long double newValue = map<long double>(5, 0, 10, -1.0, 0.0);
    std::cout << newValue << std::endl;
    ASSERT_NEAR(newValue, -0.5, 1.0e-15);
}

TEST(MapTest, ZeroValue) {
    long double newValue = map<long double>(0, -10, 10, -2.0, 1.0);
    std::cout << newValue << std::endl;
    ASSERT_NEAR(newValue, -0.5, 1.0e-15);
}