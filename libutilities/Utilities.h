//
// Created by steve on 9/10/19.
//

#ifndef MANDLEBROT_UTILITIES_H
#define MANDLEBROT_UTILITIES_H

#include <iostream>
#include <cstdlib>

template <typename T>
T map(T value, T fromStart, T fromEnd, T toStart, T toEnd) {

    T howFar = std::abs(fromStart - value);

    return ((std::abs(fromStart - value) / (fromEnd - fromStart)) * std::abs(toEnd - toStart)) + toStart;
}

#endif //MANDLEBROT_UTILITIES_H
