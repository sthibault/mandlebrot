//
// Created by steve on 9/3/19.
//

#ifndef MANDLEBROT_COMPLEX_H
#define MANDLEBROT_COMPLEX_H

#include <iostream>

class Complex {
public:
    Complex(long double r, long double i);

    // Math Operator implementations
    Complex operator+(const Complex& c) const;
    Complex operator-(const Complex& c) const;
    Complex operator*(const Complex& c) const;
    Complex operator/(const Complex& c) const;

    // Relations
    bool operator==(const Complex& c) const;
    bool operator!=(const Complex& c) const;

    // Copy, Move and Assign Operators
    Complex& operator+=(const Complex& c);
    Complex& operator-=(const Complex& c);
    Complex& operator*=(const Complex& c);
    Complex& operator/=(const Complex& c);

    // Streams
    friend std::ostream& operator<<(std::ostream& os, const Complex& c);
    friend std::istream& operator>>(std::istream& is, Complex& c);

    // Properties
    long double magnitude() const;
    long double theta() const;
    long double real() const;
    long double imaginary() const;

private:
    Complex();
    std::string toString() const;
    Complex multiplyByFOIL(const Complex &rhs, const Complex &lhs) const;
    Complex multiplyByRTheta(const Complex &rhs, const Complex &lhs) const;
    Complex divideByConjugate(const Complex &rhs, const Complex &lhs) const;

    long double _real;
    long double _imaginary;

};


#endif //MANDLEBROT_COMPLEX_H
