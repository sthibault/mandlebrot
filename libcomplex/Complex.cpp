//
// Created by steve on 9/3/19.
//

#include "Complex.h"
#include <sstream>
#include <cmath>

Complex::Complex(): _real{0.0}, _imaginary{0.0}
{}

Complex::Complex(long double r, long double i): _real{r}, _imaginary{i}
{}

Complex Complex::operator+(const Complex& c) const {
    Complex result(c._real + _real, c._imaginary + _imaginary);
    return result;
}

Complex Complex::operator-(const Complex& c) const {
    Complex result(_real - c._real, _imaginary - c._imaginary);
    return result;
}

Complex Complex::operator*(const Complex &c) const {
    //return multiplyByRTheta(c);
    return multiplyByFOIL(*this, c);
}

Complex Complex::multiplyByFOIL(const Complex &lhs, const Complex &rhs) const {
    Complex result(((lhs._real* rhs.real()) - (lhs._imaginary * rhs.imaginary())),
            ((lhs._real * rhs.imaginary()) + (lhs._imaginary * rhs.real())));

    return result;
}

Complex Complex::multiplyByRTheta(const Complex& lhs, const Complex &rhs) const {
    long double m = lhs.magnitude() * rhs.magnitude();
    long double t = lhs.theta() + rhs.theta();

    Complex result( m * cos(t), m * sin(t));

    return result;
}

Complex Complex::operator/(const Complex &c) const {
    return divideByConjugate(*this, c);
}

Complex Complex::divideByConjugate(const Complex &lhs, const Complex & rhs) const {
    long double r = (lhs._real * rhs.real()) + (lhs._imaginary * rhs.imaginary())
                     /(powl(rhs.real(), 2) + powl(rhs.imaginary(), 2));
    long double i = (lhs._imaginary * rhs.real()) - (lhs._real * rhs.imaginary())
                     /(powl(rhs.real(), 2) + powl(rhs.imaginary(), 2));

    return Complex(r, i);
}

bool Complex::operator==(const Complex& c) const {
    return ((_real == c._real) && (_imaginary == c._imaginary));
}

bool Complex::operator!=(const Complex &c) const {
    return !operator==(c);
}

Complex& Complex::operator+=(const Complex& c) {
    _real += c.real();
    _imaginary += c.imaginary();

    return *this;
}

Complex& Complex::operator-=(const Complex& c) {
    _real -= c.real();
    _imaginary -= c.imaginary();

    return *this;
}

Complex& Complex::operator*=(const Complex& c) {
    Complex result = *this * c;

    _real = result._real;
    _imaginary = result._imaginary;

    return *this;
}

Complex& Complex::operator/=(const Complex& c) {
    Complex result = *this / c;

    _real = result._real;
    _imaginary = result._imaginary;

    return *this;
}

long double Complex::magnitude() const {
    return sqrtl((_real * _real) + (_imaginary * _imaginary));
}

long double Complex::theta() const {
    return atan2l(_imaginary, _real);
}

long double Complex::real() const {
    return _real;
}

long double Complex::imaginary() const {
    return _imaginary;
}

std::string Complex::toString() const {
    std::stringstream stream;
    stream << real() << " + " << imaginary() << "i";

    return stream.str();
}

std::ostream& operator<<(std::ostream& os, const Complex& c) {
    os << c.toString();
    return os;
}

std::istream& operator>>(std::istream& is, Complex& c) {
    std::string temp;
    is >> c._real >> temp >> c._imaginary >> temp;
    return is;
}
