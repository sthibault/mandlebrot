//
// Created by steve on 9/4/19.
//

#include "libcomplex/Complex.h"
#include "gtest/gtest.h"
#include <limits>
#include <cmath>

TEST(ComplexTest, Test_Construtor) {
    Complex c(1, 2);
    ASSERT_EQ(1, c.real())
        << "The Complex Real part was not set";
    ASSERT_EQ(2, c.imaginary())
        << "The Complex Imaginary part was not set";
}

TEST(ComplexTest, Test_Add_Operator) {
    Complex c1(1, 3);
    Complex c2(2, 5);

    Complex result = c1 + c2;

    ASSERT_EQ(result.real(), 3)
        << "Complex Addition between 1 + 3i and 2 + 5i, the real part should be 8";
    ASSERT_EQ(result.imaginary(), 8)
        << "Complex Addition between 1 + 3i and 2 + 5i, the imaginry part should be 8";
}

TEST(ComplexTest, Test_Subtract_Operator) {
    Complex c1(0, 1);
    Complex c2(1, 1);

    Complex result = c1 - c2;

    ASSERT_NEAR(result.real(), -1, 1e-15)
                                << "Complex subtraction between i and 1 + 1i should be -1";

    ASSERT_NEAR(result.imaginary(), 0, 1e-15)
                                << "Complex subtraction between i and 1 + 1i should be -1";
}


TEST(ComplexTest, Test_Multiply_Operator_i_and_i) {
    Complex c1(0, 1);
    Complex c2(0, 1);

    Complex result = c1 * c2;

    ASSERT_NEAR(result.real(), -1,
            1e-15)
            << "Complex Multiplication between i and i should be -1";
    ASSERT_NEAR(result.imaginary(), 0,
            1e-15)
            << "Complex Multiplication between i and i should be -1";
}

TEST(ComplexTest, Test_Multiply_Operator_i_and_1) {
    Complex c1(0, 1);
    Complex c2(1, 0);

    Complex result = c1 * c2;

    ASSERT_NEAR(result.real(), 0,
            1e-15)
            << "Complex Multiplication between i and 1 should be i";

    ASSERT_NEAR(result.imaginary(), 1,
            1e-15)
            << "Complex Multiplication between i and 1 should be i";
}

TEST(ComplexTest, Test_Multiply_Operator_2_3i_and_1_5i) {
    Complex c1(2, 3);
    Complex c2(1, 5);

    Complex result = c1 * c2;

    ASSERT_NEAR(result.real(), -13,
                1e-14)
                 << "Complex Multiplication between 2 + 3i and 1 + 5i should be -13 + 13i";

    ASSERT_NEAR(result.imaginary(), 13,
                1e-15)
                << "Complex Multiplication between 2 + 3i and 1 + 5i should be -13 + 13i";
}

TEST(ComplexTest, Test_Magnitude) {
    Complex c1(3, 4);

    long double m = c1.magnitude();

    ASSERT_NEAR(c1.magnitude(), 5, 1e-14)
                << "Complex magnitude 3 + 4i should be 5";
}

TEST(ComplexTest, Test_Theta_1_1i) {
    Complex c(1, 1);

    long double theta = c.theta();

    ASSERT_NEAR(theta, M_PI_4l, 1e-15)
                << "Complex theta of 1 + 1i should be 45 deg, or PI/4";
}

TEST(ComplexTest, Test_Theta_1_0i) {
    Complex c(1, 0);

    long double theta = c.theta();

    ASSERT_NEAR(theta, 0, 1e-15)
                                << "Complex theta of 1 + 0i should be 0";
}

TEST(ComplexTest, Test_Theta_0_1i) {
    Complex c(0, 1);

    long double theta = c.theta();

    ASSERT_NEAR(theta, M_PI_2l, 1e-15)
                                << "Complex theta of 1 + 1i should be 90 deg, or PI/2";
}

TEST(ComplexTest, Test_Theta_1_minus_1i) {
    Complex c(1, -1);

    long double theta = c.theta();

    ASSERT_NEAR(theta, -M_PI_4l, 1e-15)
                                << "Complex theta of 1 + 1i should be 90 deg, or PI/2";
}

TEST(ComplexTest, Test_Division_1_1i_and_i) {
    Complex c1(1, 1);
    Complex c2(0, 1);

    Complex result = c1 / c2;

    ASSERT_NEAR(result.theta(), -M_PI_4l, 1e-15)
                << "Complex division between 1 - i and i should be -PI/4";
}

TEST(ComplexTest, Test_Plus_Equals_Operator) {
    Complex result(1, 3);
    Complex c2(2, 5);

    result += c2;

    ASSERT_EQ(result.real(), 3)
                                << "Complex Addition between 1 + 3i and 2 + 5i, the real part should be 8";
    ASSERT_EQ(result.imaginary(), 8)
                                << "Complex Addition between 1 + 3i and 2 + 5i, the imaginry part should be 8";
}

TEST(ComplexTest, Test_Subtract_Equals_Operator) {
    Complex result(0, 1);
    Complex c2(1, 1);

    result -= c2;

    ASSERT_NEAR(result.real(), -1, 1e-15)
                                << "Complex subtraction between i and 1 + 1i should be -1";

    ASSERT_NEAR(result.imaginary(), 0, 1e-15)
                                << "Complex subtraction between i and 1 + 1i should be -1";
}

TEST(ComplexTest, Test_Multiply_Equals_Operator) {
    Complex result(2, 3);
    Complex c(1, 5);

    result *= c;

    ASSERT_NEAR(result.real(), -13,
                1e-14)
                 << "Complex Multiplication between 2 + 3i and 1 + 5i should be -13 + 13i";

    ASSERT_NEAR(result.imaginary(), 13,
                1e-15)
                 << "Complex Multiplication between 2 + 3i and 1 + 5i should be -13 + 13i";
}

TEST(ComplexTest, Test_Divide_Equals_Operator) {
    Complex result(1, 1);
    Complex c1(0, 1);

    result /= c1;

    std::cout << result << std::endl;
    ASSERT_NEAR(result.theta(), -M_PI_4l, 1e-15)
                                << "Complex division between 1 - i and i should be -PI/4";
}

TEST(ComplexTest, TesT_equality_operator_should_be_equal_i_and_i) {
    Complex c1(0, 1);
    Complex c2(0, 1);

    ASSERT_TRUE(c1 == c2)
                << "i should equal i";
}

TEST(ComplexTest, TesT_equality_operator_should_be_equal_1_and_1) {
    Complex c1(1, 0);
    Complex c2(1, 0);

    ASSERT_TRUE(c1 == c2)
                << "1 should equal 1";
}

TEST(ComplexTest, Test_equality_operator_should_not_be_equal_1_and_i) {
    Complex c1(1, 0);
    Complex c2(0, 1);

    ASSERT_FALSE(c1 == c2)
                 << "1 should not equal i";
}

TEST(ComplexTest, Test_inequality_operator_should_be_equal_i_and_i) {
    Complex c1(0, 1);
    Complex c2(0, 1);

    ASSERT_FALSE(c1 != c2)
                                << "i != i should be false";
}

TEST(ComplexTest, TesT_inequality_operator_should_be_equal_1_and_1) {
    Complex c1(1, 0);
    Complex c2(1, 0);

    ASSERT_FALSE(c1 != c2)
                                << "1 != 1 should be false";
}

TEST(ComplexTest, TesT_inequality_operator_should_not_be_equal_1_and_i) {
    Complex c1(1, 0);
    Complex c2(0, 1);

    ASSERT_TRUE(c1 != c2)
                                << "1 != i should be true";
}
