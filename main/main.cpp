#include "Game.h"
#include "libcomplex/Complex.h"
#include "libutilities/Utilities.h"
#include <iostream>
#include <vector>

const int xmax = 2000;
const int ymax = 1000;


int main() {
    //Game sdl("Mandlebrot");

    int max = 100;
    std::vector<std::vector<int>> grid(ymax, std::vector<int>(xmax, 0));


    for (int y = 0; y < xmax; ++y) {
        for (int x = 0; x < xmax; ++x) {
            auto real = map<long double>(x, 0, xmax - 1, -2.0, 1.0);
            auto imaginary = map<long double>(y, 0, ymax - 1, -1, 1.0);
            Complex c(real, imaginary);
            Complex z(0, 0);
            int iteration = 0;

            while (z.magnitude() <= 2 && iteration++ < max){
                z = z * z + c;
            }

            if (iteration >= max) {
                grid[y][x] = 1;
            }
        }
    }

    return 0;
}