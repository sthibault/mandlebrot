//
// Created by steve on 9/11/19.
//

#ifndef MANDLEBROT_GAME_H
#define MANDLEBROT_GAME_H

#include "GameError.h"

#include <string>
#include <unordered_map>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

struct Color {
    Uint8 red;
    Uint8 green;
    Uint8 blue;
    Uint8 alpha = SDL_ALPHA_OPAQUE;
};

class Game {
public:
    explicit Game(std::string title);
    ~Game();
    std::string openFont(std::string fontPath, int size);
    SDL_Rect drawableArea() const;
    SDL_Event pollEvent() const;
    void clear(Color c) const;
    void present() const;
    void drawText(std::string font, SDL_Rect area, std::string text);
    Color drawColor() const;
    void drawColor(Color c);
    void exitGame();
    //extern DECLSPEC int SDLCALL SDL_RenderDrawPoint(SDL_Renderer * renderer,
    //                                                int x, int y);
    void drawPoint(int x, int y);

private:
    void init(std::string title);
    void logError(std::ostream &os, const GameError& gameError);

    SDL_Window *window = nullptr;
    SDL_Renderer *renderer = nullptr;
    //std::unordered_map<std::string, std::unordered_map<int, TTF_Font>> fonts{};
    SDL_Rect windowBounds{};
    SDL_Rect displayBounds{};
    std::string title{};
    std::string lastError{};
};


#endif //MANDLEBROT_GAME_H
