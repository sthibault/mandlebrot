//
// Created by steve on 9/11/19.
//

#ifndef MANDLEBROT_GAMEERROR_H
#define MANDLEBROT_GAMEERROR_H
#include <string>
#include <exception>
#include <SDL.h>
#include <stdexcept>


struct GameError : std::runtime_error {
    GameError(const std::string& source) :
        runtime_error{"GameError"}, source{source}, message{SDL_GetError()} {};
    GameError(const std::string& source, const std::string& message) :
        runtime_error("GameError"), source{source}, message{message} {};
    std::string source;
    std::string message;
};


#endif //MANDLEBROT_GAMEERROR_H
