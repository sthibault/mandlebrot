//
// Created by steve on 9/11/19.
//

#include "Game.h"
#include "sdlcleanup.h"
#include <SDL.h>
#include <utility>
#include <iostream>

Game::Game(std::string title) {
    init(std::move(title));

}

Game::~Game() {
    exitGame();
    //quit(TTF_Quit, IMG_Quit, SDL_Quit);
}

void Game::init(std::string title) {
    try {
        int result = SDL_Init(SDL_INIT_EVERYTHING);
        if (result != 0) {
            throw GameError{"SDL_Init"};
        }

        result = IMG_Init(IMG_INIT_PNG);
        if ((result & IMG_INIT_PNG) != IMG_INIT_PNG) {
            throw GameError{"IMG_Init"};
        }

        result = TTF_Init();
        if (result == -1) {
            throw GameError{"TTF_Init"};
        }

        SDL_GetDisplayUsableBounds(0, &displayBounds);
        window = SDL_CreateWindow(title.c_str(),
                                  displayBounds.x, displayBounds.y, displayBounds.w, displayBounds.h,
                SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        if (window == nullptr) {
            throw GameError{"SDL_CreateWindow"};
        }

        renderer = SDL_CreateRenderer(window, -1,
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (renderer == nullptr) {
            throw GameError{"SDL_CreateRenderer"};
        }

        // TODO: Load Fonts
    }
    catch (GameError gameError) {
        logError(std::cout, gameError);
        throw;
    }
}

SDL_Rect Game::drawableArea() const {
    int width;
    int height;

    SDL_GL_GetDrawableSize(window, &width, &height);
}

SDL_Event Game::pollEvent() const {
    SDL_Event event;
    SDL_PollEvent(&event);
    return event;
}

void Game::clear(Color color) const {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
    SDL_RenderClear(renderer);
}

void Game::present() const {
    SDL_RenderPresent(renderer);
}

Color Game::drawColor() const {
    Color color;
    if (SDL_GetRenderDrawColor(renderer, &color.red, &color.green,
            &color.blue, &color.alpha) != 0) {
        throw GameError("SDL_GetRendererDrawColor");
    }

    return color;
}

void Game::drawColor(Color color) {
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
}

void Game::exitGame() {
    cleanup(renderer, window);
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void Game::logError(std::ostream &os, const GameError& gameError) {
    os << gameError.source << " error: " << gameError.message << std::endl;
}

